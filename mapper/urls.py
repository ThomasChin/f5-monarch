"""mapper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from mapper.views import MapperSyncView

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/bugzilla", include("mapper.bugzilla.urls", namespace="bugzilla")),
    path("api/v1/jira", include("mapper.jira.urls", namespace="jira")),
    path("api/v1/logs", include("mapper.logs.urls", namespace="logs")),
    path("api/v1/sync", MapperSyncView.as_view()),
]
