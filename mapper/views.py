import datetime
from jira import JIRAError
from rest_framework.views import APIView
from rest_framework.response import Response

from mapper.bugzilla.helpers import (
    bz_update_jira,
    get_comments_threading,
    get_latest_bugs,
    parse_bugs,
    post_all_bugs_threading,
    add_bugzilla_bug_ids_to_sync_log,
)
from mapper.bugzilla.models import BugzillaTicket
from mapper.logs.models import SyncFrequency, SyncLog
from mapper.jira.models import JiraProject, JiraTicket
from mapper.jira.utils import (
    convert_bugzilla_issues,
    get_latest_issues,
    monarch_test_jira,
    parse_issues,
    separate_jira_tickets,
)


class MapperSyncView(APIView):
    def get(self, request, *args, **kwargs):
        sync_frequency = SyncFrequency.objects.get(
            name="Default"
        ).frequency_by_minutes  # Default is 15 minutes.

        last_sync_time = datetime.datetime.now() - datetime.timedelta(
            minutes=int(sync_frequency)
        )
        synclog = SyncLog.objects.create()

        ### JIRA GET
        latest_issues = get_latest_bugs()
        parsed_issues = parse_bugs(latest_issues, last_sync_time)
        parsed_issues_with_comments = get_comments_threading(
            parsed_issues=parsed_issues
        )
        separated_tickets = separate_jira_tickets(parsed_issues_with_comments, synclog)
        new_tickets = convert_bugzilla_issues(separated_tickets["new"])
        update_tickets = convert_bugzilla_issues(separated_tickets["update"])

        default_project = str(JiraProject.objects.get(name="Default").project_name)

        new_mtp_tickets = [
            ticket for ticket in new_tickets if ticket["project_key"] == default_project
        ]
        update_mtp_tickets = [
            ticket
            for ticket in update_tickets
            if ticket["project_key"] == default_project
        ]

        ### BUGZILLA GET
        jira_latest_bugs = get_latest_issues(jira_project=monarch_test_jira)
        parsed_jira_bugs = parse_issues(
            jira_project=monarch_test_jira, issues_in_project=jira_latest_bugs
        )

        jira_bugs_in_bugzilla_tickets_form = add_bugzilla_bug_ids_to_sync_log(
            parsed_jira_bugs, synclog
        )

        ### JIRA POST AND UPDATE
        # custom field IDs for Jira are found in the Jira administration site.
        for issue in new_mtp_tickets:
            issue_data = {
                "project": {"key": issue["project_key"]},
                "issuetype": {"name": issue["issue_type"]},
                "priority": {"name": issue["priority"]},
                "description": issue["description"],
                "summary": "bugzilla " + issue["bugzilla_key"],
                "customfield_10028": str(issue["bugzilla_key"]),
                "customfield_10027": str(issue["version"]),
                "customfield_10029": str(issue["component"]),
            }
            issue_list = [issue_data]

            if issue["issue_type"] == "Epic":
                issue_data["customfield_10011"] = issue_data["summary"]

            try:
                responses = monarch_test_jira.create_issues(field_list=issue_list)
                for comment in issue["comments"]:
                    monarch_test_jira.add_comment(responses[0]["issue"], comment)
                for response in responses:
                    bz_id = response["input_fields"]["customfield_10028"]
                    jira_id = response["issue"]
                    bz_update_jira(bz_id=bz_id, jira_id=jira_id)
            except JIRAError as e:
                synclog.success = False
                synclog.error_notes += str(e)

        for issue in update_mtp_tickets:
            try:
                jira_issue = monarch_test_jira.issue(issue["jira_key"])
                jira_issue.update(
                    fields={
                        "project": {"key": issue["project_key"]},
                        "issuetype": {"name": issue["issue_type"]},
                        "priority": {"name": issue["priority"]},
                        "description": issue["description"],
                        "summary": "bugzilla " + issue["bugzilla_key"],
                        "customfield_10028": str(issue["bugzilla_key"]),
                        "customfield_10027": str(issue["version"]),
                        "customfield_10029": str(issue["component"]),
                    }
                )
                for comment in issue["comments"]:
                    monarch_test_jira.add_comment(issue["jira_key"], comment)
            except JIRAError as e:
                synclog.success = False
                synclog.error_notes += str(e)

        # BUGZILLA POST AND UPDATE
        bugzilla_log = post_all_bugs_threading(jira_bugs_in_bugzilla_tickets_form)

        for bug in bugzilla_log:
            if bug["success_sync"] == False:
                synclog.success = False
                synclog.error_notes = bug["error_notes"]

        # Final Cleanup
        synclog.finish_time = datetime.datetime.now()
        synclog.save()
        JiraTicket.objects.all().delete()
        BugzillaTicket.objects.all().delete()

        return Response(
            {
                "jira": {"new": new_mtp_tickets, "update": update_mtp_tickets},
                "bugzilla": bugzilla_log,
            }
        )
