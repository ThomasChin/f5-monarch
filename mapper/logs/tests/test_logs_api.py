from django.test import Client
import pytest

from conftest import admin_client

LOGS_URL = "/api/v1/logs"


@pytest.mark.django_db
def test_list_sync_logs(admin_client: Client, testing_synclog):
    """
	Tests that an admin can view the list of SyncLogs.
	"""
    r = admin_client.get(LOGS_URL)
    assert 200 == r.status_code


def test_cannot_access_sync_log_list_if_not_admin(client: Client):
    """
	Tests that only admins can view the list of SyncLogs.

	Args:
		client: Standard test client (no admin privileges).
	"""
    r = client.get(LOGS_URL)
    assert 403 == r.status_code
