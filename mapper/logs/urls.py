from django.urls import path

from mapper.logs.views import ListSyncLogsView

app_name = "logs"

urlpatterns = [
    path("", ListSyncLogsView.as_view()),
]
