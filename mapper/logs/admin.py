from django.contrib import admin
from .models import SyncFrequency, SyncLog


class SyncFrequencyAdmin(admin.ModelAdmin):
    model = SyncFrequency
    list_display = ("name", "frequency_by_minutes")


class SyncLogAdmin(admin.ModelAdmin):
    model = SyncLog
    list_display = (
        "created",
        "finish_time",
        "success",
        "error_notes",
        "new_bugzilla_to_jira_ids",
        "existing_bugzilla_to_jira_ids",
        "new_jira_to_bugzilla_ids",
        "existing_jira_to_bugzilla_ids",
    )


admin.site.register(SyncFrequency, SyncFrequencyAdmin)
admin.site.register(SyncLog, SyncLogAdmin)
