from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAdminUser

from mapper.logs.models import SyncLog
from mapper.logs.serializers import SyncLogSerializer


class ListSyncLogsView(ListAPIView):
    queryset = SyncLog.objects.all()
    serializer_class = SyncLogSerializer
    permission_classes = [IsAdminUser]
