from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import IntegerField

from mapper.models import TimeStampedModel


class SyncFrequency(models.Model):
    frequency_by_minutes = IntegerField(default=1440)
    name = models.CharField(max_length=2048, null=True)


class SyncLog(TimeStampedModel):
    new_bugzilla_to_jira_ids = ArrayField(
        models.CharField(max_length=2048, blank=True), null=True, size=100
    )
    existing_bugzilla_to_jira_ids = ArrayField(
        models.CharField(max_length=2048, blank=True), null=True, size=100
    )
    new_jira_to_bugzilla_ids = ArrayField(
        models.CharField(max_length=2048, blank=True), null=True, size=100
    )
    existing_jira_to_bugzilla_ids = ArrayField(
        models.CharField(max_length=2048, blank=True), null=True, size=100
    )
    finish_time = models.DateTimeField(null=True)
    success = models.BooleanField(default=True)
    error_notes = models.TextField(default="", blank=True)
