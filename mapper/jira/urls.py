from django.urls import path

from .views import GetLatestIssuesView

app_name = "jira"

urlpatterns = [
    path("/<int:jira>/changes", GetLatestIssuesView.as_view()),
    path("/<int:jira>/changes/<str:days_since>", GetLatestIssuesView.as_view()),
]
