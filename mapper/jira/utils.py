import jira
import os
import datetime

from mapper.bugzilla.serializers import ToJiraIssueSerializer
from mapper.jira.models import JiraTicket

USERNAME = os.environ.get("JIRA_USER")
TOKEN = os.environ.get("JIRA_TOKEN")
JIRA_URL = os.environ.get("JIRA_URL")

monarch_jira = jira.JIRA(basic_auth=(USERNAME, TOKEN), options={"server": JIRA_URL})

TEST_USERNAME = os.environ.get("TEST_HARNESS_JIRA_USER")
TEST_TOKEN = os.environ.get("TEST_HARNESS_JIRA_TOKEN")
TEST_JIRA_URL = os.environ.get("TEST_HARNESS_JIRA_URL")

monarch_test_jira = jira.JIRA(
    basic_auth=(TEST_USERNAME, TEST_TOKEN), options={"server": TEST_JIRA_URL}
)

jira_keys = {0: monarch_test_jira, 1: monarch_jira}

name_id_map = {
    monarch_test_jira.field["name"]: monarch_test_jira.field["id"]
    for monarch_test_jira.field in monarch_test_jira.fields()
}


def update_jira_mirror_id(jira_id, bz_id):
    """
    Updates a Jira issue with a mirror id from Bugzilla.
    """
    jira_issue = monarch_test_jira.issue(jira_id)
    # custom field id varies by Jira project.
    jira_issue.update(
        fields={"customfield_10028": str(bz_id),}
    )


def get_latest_issues(jira_project=monarch_test_jira):
    """
    Returns instances of all issues that have been updated in the specified time frame.

    Args:
        jira_project: Specified project in Jira.
    """
    issues = jira_project.search_issues(jql_str="updated > -15m and project=MT2")
    return issues


def parse_issues(
    issues_in_project, last_sync_time=None, jira_project=monarch_test_jira,
):
    """
    Parses Jira issues and stores data in a dictionary.

    Args:
        issues_in_project: Jira issues.
        last_sync_time: Finish time of the last recorded Sync
        jira_project: Specified project in Jira.
    """
    issues = {}
    if issues_in_project:
        for issue in issues_in_project:
            # get time of last update for issue
            issue_last_updated_time = issue.fields.updated
            formatted_last_updated_time = datetime.datetime(
                int(issue_last_updated_time[:4]),
                int(issue_last_updated_time[5:7]),
                int(issue_last_updated_time[8:10]),
                int(issue_last_updated_time[11:13]),
                int(issue_last_updated_time[14:16]),
                int(issue_last_updated_time[17:19]),
            )

            check_sync_time = True

            # if issue hasn't been updated since last sync, no need to re-sync it.
            if last_sync_time:
                if last_sync_time >= formatted_last_updated_time:
                    check_sync_time = False

            if check_sync_time:
                issue_fields = {
                    "project_key": str(issue.fields.project.key),
                    "issue_key": str(issue.key),
                    "priority": str(issue.fields.priority.name),
                    "issue_type": str(issue.fields.issuetype),
                    "description": str(issue.fields.description),
                    "component": str(
                        getattr(issue.fields, name_id_map["Component"], "")
                    ),
                    "version": str(getattr(issue.fields, name_id_map["Version"], "")),
                    "bz_id": str(
                        getattr(issue.fields, name_id_map["Bugzilla Key"], "")
                    ),
                }
                comment_list = jira_project.comments(issue)
                issue_fields["comments"] = []

                if len(comment_list) > 0:
                    for comment in comment_list:
                        formatted_comment_time = datetime.datetime(
                            int(comment.created[:4]),
                            int(comment.created[5:7]),
                            int(comment.created[8:10]),
                            int(comment.created[11:13]),
                            int(comment.created[14:16]),
                            int(comment.created[17:19]),
                        )
                        if last_sync_time:
                            if last_sync_time < formatted_comment_time:
                                issue_fields["comments"].append(str(comment.body))
                        else:
                            if (
                                datetime.datetime.now() - datetime.timedelta(minutes=15)
                                < formatted_comment_time
                            ):
                                issue_fields["comments"].append(str(comment.body))

                issues[str(issue.key)] = issue_fields

    return issues


def separate_jira_tickets(bugzilla_json, synclog):
    """
    Separates Bugzilla bugs into lists depending on if they already exist in Jira or not.

    Args:
        bugzilla_json: Bugzilla json data.
        synclog: Log for keeping track of bug ID information during this sync.
    """
    new_tickets = [ticket for ticket in bugzilla_json if ticket["cf_jira_bug"] == ""]
    update_tickets = [ticket for ticket in bugzilla_json if ticket not in new_tickets]
    synclog.new_bugzilla_to_jira_ids = [ticket["id"] for ticket in new_tickets]
    synclog.existing_bugzilla_to_jira_ids = [ticket["id"] for ticket in update_tickets]
    return {"new": new_tickets, "update": update_tickets}


def convert_bugzilla_issues(parsed_issues):
    """
    Takes parsed Bugzilla data and creates Jira objects to be serialized.

    Args:
        parsed_issues: Parsed bug data from Bugzilla.
    """
    # Convert Priority Naming
    for issue in parsed_issues:
        if issue["priority"] == "Normal":
            issue["priority"] = "Medium"

    tickets = [
        JiraTicket.objects.create(
            project_key=issue["product"],
            component=issue["component"],
            description=issue["summary"],
            version=issue["version"],
            priority=issue["priority"],
            comments=issue["comments"],
            issue_type=issue["cf_issue_type"],
            bugzilla_key=issue["id"],
            jira_key=issue["cf_jira_bug"],
        )
        for issue in parsed_issues
    ]

    return [ToJiraIssueSerializer(ticket).data for ticket in tickets]
