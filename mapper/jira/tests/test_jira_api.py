from django.test import Client
import jira
import pytest

from mapper.jira.models import JiraTicket
from mapper.jira.utils import (
    convert_bugzilla_issues,
    get_latest_issues,
    monarch_test_jira,
    separate_jira_tickets,
    update_jira_mirror_id,
)
from mapper.logs.models import SyncLog


BASE_TEST_URL = "/api/v1/jira/0/changes"
BASE_PROD_URL = "/api/v1/jira/1/changes"
BASE_URL = "/api/v1/jira"

TEST_MIRROR_ID_JIRA_KEY = "MPY-1"


def test_get_latest_issues_endpoint_response(client: Client):
    """
    Tests that the latest-issues endpoint for Jira returns 200 status code.

    Args:
        client: Standard Django test client used for making API requests to Mapper endpoints.
    """
    test_response = client.get(BASE_TEST_URL)
    assert 200 == test_response.status_code


@pytest.mark.parametrize("payload", ["test_bugzilla_id", "", None])
def test_update_jira_mirror_id(payload):
    """
    Tests the 'update_jira_mirror_id' utility function to ensure it is correctly altering the 'Bugzilla Key' field on
    a Jira issue.

    Args:
        payload: A payload consisting of one string or None to set the 'Bugzilla Key' field to.
    """
    update_jira_mirror_id(TEST_MIRROR_ID_JIRA_KEY, payload)
    jira_issue = monarch_test_jira.issue(
        TEST_MIRROR_ID_JIRA_KEY, fields="customfield_10028"
    )
    assert jira_issue is not None

    if payload == "":
        assert "None" == str(jira_issue.fields.customfield_10028)
    else:
        assert str(payload) == str(jira_issue.fields.customfield_10028)


def test_get_latest_issues_response_type():
    """
    Test that checks that the utility function 'get_latest_issues' returns the correct type of response from Jira.
    (A ResultList of jira issues)
    """
    latest_issues_response = get_latest_issues(jira_project=monarch_test_jira)
    assert jira.client.ResultList == type(latest_issues_response)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payload, expected",
    [
        (
            [
                {
                    "id": 956,
                    "priority": "Lowest",
                    "product": "MTP",
                    "component": "Test Component",
                    "summary": "This is a created bug on april 22nd",
                    "version": "unspecified",
                    "cf_issue_type": "Bug",
                    "cf_jira_bug": "MTP-203",
                    "last_change_time": "2020-04-27T03:23:58Z",
                    "creation_time": "2020-04-22T16:19:10Z",
                    "comments": [],
                },
                {
                    "id": 960,
                    "priority": "High",
                    "product": "MTP",
                    "component": "Test Component",
                    "summary": "april 26 night",
                    "version": "unspecified",
                    "cf_issue_type": "Bug",
                    "cf_jira_bug": "",
                    "last_change_time": "2020-04-27T03:24:21Z",
                    "creation_time": "2020-04-27T03:24:21Z",
                    "comments": [("comment one", "Projectmonarch20.07@gmail.com")],
                },
            ],
            {
                "new": [
                    {
                        "id": 960,
                        "priority": "High",
                        "product": "MTP",
                        "component": "Test Component",
                        "summary": "april 26 night",
                        "version": "unspecified",
                        "cf_issue_type": "Bug",
                        "cf_jira_bug": "",
                        "last_change_time": "2020-04-27T03:24:21Z",
                        "creation_time": "2020-04-27T03:24:21Z",
                        "comments": [("comment one", "Projectmonarch20.07@gmail.com")],
                    }
                ],
                "update": [
                    {
                        "id": 956,
                        "priority": "Lowest",
                        "product": "MTP",
                        "component": "Test Component",
                        "summary": "This is a created bug on april 22nd",
                        "version": "unspecified",
                        "cf_issue_type": "Bug",
                        "cf_jira_bug": "MTP-203",
                        "last_change_time": "2020-04-27T03:23:58Z",
                        "creation_time": "2020-04-22T16:19:10Z",
                        "comments": [],
                    }
                ],
            },
        ),
    ],
)
def test_separate_jira_tickets(payload, expected):
    """
    Tests that the 'separate_jira_tickets()' utility filters JSON data from Bugzilla properly and also logs the attempt
    correctly in the passed in SyncLog object.
    """
    log = SyncLog.objects.create()
    separated_tickets = separate_jira_tickets(payload, log)

    assert expected["new"] == separated_tickets["new"]
    assert expected["update"] == separated_tickets["update"]
    assert [issue["id"] for issue in expected["new"]] == log.new_bugzilla_to_jira_ids
    assert [
        issue["id"] for issue in expected["update"]
    ] == log.existing_bugzilla_to_jira_ids


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payload, expected",
    [
        (
            [
                {
                    "id": 956,
                    "priority": "Low",
                    "product": "MTP",
                    "component": "Test Component",
                    "summary": "This is a created bug on april 22nd",
                    "version": "unspecified",
                    "cf_issue_type": "Bug",
                    "cf_jira_bug": "MTP-203",
                    "last_change_time": "2020-04-27T00:46:20Z",
                    "creation_time": "2020-04-22T16:19:10Z",
                    "comments": [],
                },
                {
                    "id": 959,
                    "priority": "Normal",
                    "product": "MTP",
                    "component": "Test Component",
                    "summary": "april 26th test",
                    "version": "unspecified",
                    "cf_issue_type": "Bug",
                    "cf_jira_bug": "MTP-206",
                    "last_change_time": "2020-04-27T00:46:53Z",
                    "creation_time": "2020-04-27T00:45:50Z",
                    "comments": ["comment"],
                },
            ],
            [
                {
                    "id": 1,
                    "project_key": "MTP",
                    "issue_type": "Bug",
                    "priority": "Low",
                    "description": "This is a created bug on april 22nd",
                    "comments": [],
                    "component": "Test Component",
                    "version": "unspecified",
                    "bugzilla_key": "956",
                    "jira_key": "MTP-203",
                },
                {
                    "id": 2,
                    "project_key": "MTP",
                    "issue_type": "Bug",
                    "priority": "Medium",
                    "description": "april 26th test",
                    "comments": ["comment"],
                    "component": "Test Component",
                    "version": "unspecified",
                    "bugzilla_key": "959",
                    "jira_key": "MTP-206",
                },
            ],
        )
    ],
)
def test_convert_bugzilla_issues(payload, expected):
    """
    Tests the utility function 'convert_bugzilla_issues' with dummy data to ensure the correct serialized format is
    returned.

    Args:
        payload: Dummy data from Bugzilla in JSON format.
        expected: Pruned and deserialized data from the payload in JSON format to eventually be sent to the Jira API.
    """
    converted_issues = convert_bugzilla_issues(payload)
    assert expected == converted_issues
    assert len(expected) == JiraTicket.objects.count()
