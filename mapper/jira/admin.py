from django.contrib import admin

from mapper.jira.models import JiraProject


class JiraProjectAdmin(admin.ModelAdmin):
    model = JiraProject
    list_display = ("name", "project_name")


admin.site.register(JiraProject, JiraProjectAdmin)
