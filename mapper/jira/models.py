from django.contrib.postgres.fields import ArrayField
from django.db import models


class JiraTicket(models.Model):
    project_key = models.CharField(max_length=64)
    issue_type = models.CharField(max_length=64)
    priority = models.CharField(max_length=64)
    description = models.TextField(blank=True, max_length=2048)
    comments = ArrayField(models.CharField(max_length=2048, blank=True), size=100)
    component = models.CharField(max_length=64, blank=True)
    version = models.CharField(max_length=64, blank=True)
    bugzilla_key = models.CharField(max_length=64, blank=True)
    jira_key = models.CharField(max_length=64, blank=True)


class JiraProject(models.Model):
    name = models.CharField(max_length=64)
    project_name = models.CharField(max_length=64)
