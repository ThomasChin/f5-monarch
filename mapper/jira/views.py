from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from mapper.jira.utils import (
    jira_keys,
    get_latest_issues,
    parse_issues,
)


class GetLatestIssuesView(APIView):
    def get(self, request, *args, **kwargs):
        search_date = int(kwargs.get("days_since", -1))
        jira_key = kwargs.get("jira")
        if search_date > 0:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        latest_issues = get_latest_issues(jira_project=jira_keys[jira_key])
        parsed_issues = parse_issues(
            jira_project=jira_keys[jira_key], issues_in_project=latest_issues,
        )
        return Response(parsed_issues)
