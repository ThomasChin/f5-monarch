from rest_framework import serializers

from mapper.bugzilla.models import BugzillaTicket


class ToBugzillaIssueSerializer(serializers.ModelSerializer):
    class Meta:
        model = BugzillaTicket
        fields = "__all__"
