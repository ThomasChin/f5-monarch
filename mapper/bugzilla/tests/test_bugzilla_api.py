from django.test import Client
import pytest

from mapper.bugzilla.helpers import *
from mapper.logs.models import SyncLog

URL_LATEST_CHANGES = "/api/v1/bugzilla/changes"
URL_SYNC_BUGS = "/api/v1/bugzilla/sync"


@pytest.mark.django_db
def test_get_latest_bugs_endpoint_response(client: Client):
    """
    Test that the endpoint retrieves the latest bugs from Bugzilla and returns an OK response code.
    """
    test_response = client.get(URL_LATEST_CHANGES)
    assert 200 == test_response.status_code


@pytest.mark.django_db
def test_get_latest_bugs_endpoint_fails(client: Client):
    """
    Test that the endpoint returns a 400 if user inputs a negative number for days back.
    """
    test_response = client.get(URL_LATEST_CHANGES + "/-1")
    assert 400 == test_response.status_code


@pytest.mark.django_db
@pytest.mark.parametrize(
    # "url_suffix", ["/2", "/10", "/100"],
    "url_suffix",
    ["/2"],
)
def test_get_latest_bugs_endpoint_with_url_suffix(client: Client, url_suffix):
    """
    Tests multiple day inputs to make sure it always returns a response status of 200.
    """
    test_response = client.get(URL_LATEST_CHANGES + url_suffix)
    assert 200 == test_response.status_code


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payload, expected",
    [
        (
            "test comment",
            {"token": TOKEN, "comment": "test comment", "is_markdown": False},
        )
    ],
)
def test_comment_json(payload, expected):
    """
    Tests if comment_json() function adds comments to dictionary
    :param payload: a string comment
    :param expected: a dictionary
    """
    result = comment_json(payload)
    assert expected == result


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payload, expected",
    [
        (
            {
                "MTP-1": {
                    "project_key": "MTP",
                    "issue_key": "MTP-1",
                    "priority": "Medium",
                    "issue_type": "Bug",
                    "description": "test summary",
                    "component": "None",
                    "version": "None",
                    "bz_id": "None",
                    "comments": [],
                },
            },
            [
                {
                    "id": 1,
                    "bz_id": "None",
                    "product": "MTP",
                    "component": "None",
                    "summary": "test summary",
                    "version": "None",
                    "priority": "Medium",
                    "comments": [],
                    "bz_issue_type": "Bug",
                    "jira_counterpart": "MTP-1",
                }
            ],
        )
    ],
)
def test_convert_jira_issues(payload, expected):
    """
    Tests if this function properly converts issues
    :param payload: a regular parsed jira bug
    :param expected: a serialized bugzilla ticket
    """
    result = convert_jira_issues(payload)
    assert expected == result


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payload, expected",
    [
        (
            {
                "id": 1,
                "bz_id": "None",
                "product": "MTP",
                "component": "None",
                "summary": "test summary",
                "version": "None",
                "priority": "Medium",
                "comments": [],
                "bz_issue_type": "Bug",
                "jira_counterpart": "MTP-1",
            },
            {
                "product": "MTP",
                "component": "Test Component",
                "summary": "test summary",
                "version": "unspecified",
                "cf_jira_bug": "MTP-1",
                "cf_issue_type": "Bug",
                "priority": "Normal",
                "op_sys": "Linux",
                "platform": "PC",
            },
        )
    ],
)
def test_reformat_data(payload, expected):
    """
    Tests if reformat_data function properly changes fields

    :param payload: a serialized jira bug
    :param expected: a dictionary with proper bugzilla fields

    """
    result = reformat_data(payload)
    assert expected == result


test_data = [
    (
        [
            {
                "product": "MTP",
                "component": "Test Component",
                "summary": "test summary",
                "version": "unspecified",
                "cf_jira_bug": "MTP-1",
                "cf_issue_type": "Bug",
                "priority": "Normal",
                "op_sys": "Linux",
                "platform": "PC",
            }
        ],
        {
            "product": "Test",
            "component": "Test Component",
            "summary": "test you",
            "version": "unspecified",
            "cf_jira_bug": "MTP-1",
            "bz_issue_type": "Epic",
            "priority": "Low",
            "op_sys": "Linux",
            "platform": "PC",
        },
        {
            "product": "Test",
            "component": "Test Component",
            "summary": "test you",
            "version": "unspecified",
            "cf_jira_bug": "MTP-1",
            "cf_issue_type": "Epic",
            "priority": "Low",
            "op_sys": "Linux",
            "platform": "PC",
        },
    )
]


@pytest.mark.django_db
@pytest.mark.parametrize("bz_bug, jira_bug, expected", test_data)
def test_comparison(bz_bug, jira_bug, expected):
    """
    Tests comparison to see if different values are updated in a bugzilla bug
    :param bz_bug: a normal bugzilla bug
    :param jira_bug: a serialized jira bug
    :param expected: a bugzilla bug with jira bug values
    """
    result = comparison(bz_bug, jira_bug)
    assert expected == result


bug = [
    (
        {
            "product": "Test",
            "component": "Test Component",
            "version": "unspecified",
            "summary": "test summary",
            "priority": "Low",
            "cf_jira_bug": "",
            "cf_issue_type": "bug",
            "op_sys": "Linux",
            "platform": "PC",
        }
    )
]


@pytest.mark.django_db
@pytest.mark.parametrize("payload ", bug)
def test_create_bug(payload):
    """
    Tests if a bug is properly created
    :param payload: a new bugzilla bug with proper fields
    """
    r = create_bugs(payload)
    assert r.status_code == requests.codes.ok


def test_get_latest_bugs():
    """
    Test the 'get_latest_bugs' function to see if it gives back a
    proper type. The time will be the default time of 15 minutes
    """
    result = get_latest_bugs()
    assert type(result) == dict


new_bug = [
    (
        {
            "product": "Test",
            "id": "966",
            "component": "Test Component",
            "version": "unspecified",
            "summary": "test summary",
            "priority": "Low",
            "cf_jira_bug": "",
            "cf_issue_type": "bug",
            "op_sys": "Linux",
            "platform": "PC",
        }
    )
]


@pytest.mark.django_db
@pytest.mark.parametrize("payload ", new_bug)
def test_get_comments(payload):
    """
    Test get comments from a bug to ensure all comments are added to the retrieved bugs.
    Default time of 15 minutes
    :param payload: a payload of bugzilla bugs without comments

    """
    result = get_comments(payload)
    assert type(result["comments"]) == list


threading_bugs = [
    (
        {
            "product": "Test",
            "id": "966",
            "component": "Test Component",
            "version": "unspecified",
            "summary": "test summary",
            "priority": "Low",
            "cf_jira_bug": "",
            "cf_issue_type": "bug",
            "op_sys": "Linux",
            "platform": "PC",
        },
        {
            "product": "Test",
            "id": "968",
            "component": "Test Component",
            "version": "unspecified",
            "summary": "test summary",
            "priority": "Low",
            "cf_jira_bug": "",
            "cf_issue_type": "bug",
            "op_sys": "Linux",
            "platform": "PC",
        },
    )
]


@pytest.mark.django_db
@pytest.mark.parametrize("payload ", threading_bugs)
def test_get_comments_threading(payload):
    """
    Tests that multi-threading for comments from Bugzilla works properly
    :param payload: two bugs

    """
    result = get_comments_threading(payload, 1)
    assert len(result) == 2


comments = [(["list list list add me  "])]


@pytest.mark.django_db
@pytest.mark.parametrize("payload ", comments)
def test_add_comments(payload):
    """
    Checks if comments were added successfully
    :param payload: a string of comments

    """
    result = add_comments(966, payload)
    assert type(result) == dict


def test_get_buzilla_bug():
    """
    Checks if the type of a bug is of type dict

    """
    result = get_buzilla_bug(969)
    assert type(result) == dict


bug_data = [
    (
        {
            "product": "Test",
            "component": "Test Component",
            "summary": "test you",
            "version": "unspecified",
            "cf_jira_bug": "MTP-1",
            "cf_issue_type": "Epic",
            "priority": "Low",
            "op_sys": "Linux",
            "platform": "PC",
        }
    )
]


@pytest.mark.django_db
@pytest.mark.parametrize("payload ", bug_data)
def test_update(payload):
    """
    Test if bug 970 is properly undated
    :param payload: updated bug data
    """
    result = update(970, payload)
    assert type(result) == dict


jira_bug_create = [
    (
        {
            "id": 1,
            "bz_id": "None",
            "product": "MTP",
            "component": "None",
            "summary": "test summary",
            "version": "None",
            "priority": "Medium",
            "comments": [],
            "bz_issue_type": "Bug",
            "jira_counterpart": "MTP-1",
        }
    )
]


@pytest.mark.django_db
@pytest.mark.parametrize("payload ", jira_bug_create)
def test_post_all_bug_creating_bug(payload):
    """
    Test if a bug is properly created and no errors are thrown.
    :param payload: jira bug data
    """
    result = post_all_bugs(payload)
    assert result["success_sync"] == True
    assert result["sync_info"] == "created"


jira_bug_update = [
    (
        {
            "id": 1,
            "bz_id": "971",
            "product": "MTP",
            "component": "None",
            "summary": "test summary",
            "version": "None",
            "priority": "Medium",
            "comments": [],
            "bz_issue_type": "Bug",
            "jira_counterpart": "MTP-1",
        }
    )
]


@pytest.mark.django_db
@pytest.mark.parametrize("payload ", jira_bug_update)
def test_post_all_bug_update_bug(payload):
    """
    Test if a already existing bug is updated
    :param payload: a jira bug
    """
    result = post_all_bugs(payload)
    assert result["success_sync"] == True
    assert result["sync_info"] == "updated"


jira_bug_list = [
    (
        [
            {
                "id": 1,
                "bz_id": "None",
                "product": "MTP",
                "component": "None",
                "summary": "test summary",
                "version": "None",
                "priority": "Medium",
                "comments": [],
                "bz_issue_type": "Bug",
                "jira_counterpart": "MTP-1",
            },
            {
                "id": 2,
                "bz_id": "971",
                "product": "MTP",
                "component": "None",
                "summary": "test summary",
                "version": "None",
                "priority": "Medium",
                "comments": [],
                "bz_issue_type": "Bug",
                "jira_counterpart": "MTP-1",
            },
        ]
    )
]


@pytest.mark.django_db
@pytest.mark.parametrize("payload ", jira_bug_list)
def test_post_all_bugs_threading(payload):
    """
    Test post-threading works properly
    :param payload: a list of two jira bugs

    """
    result = post_all_bugs_threading(payload)
    assert len(result) == 2
    assert type(result) == list


def test_bz_update_jira():
    """
    Tests if bug 978 adds a jira ID.
    """
    result = bz_update_jira(978, "MTP-1")
    assert type(result) == dict


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payload, expected",
    [
        (
            {
                "MTP-1": {
                    "project_key": "MTP",
                    "issue_key": "MTP-1",
                    "priority": "Medium",
                    "issue_type": "Bug",
                    "description": "test summary",
                    "component": "None",
                    "version": "None",
                    "bz_id": "None",
                    "comments": [],
                },
                "MTP-2": {
                    "project_key": "MTP",
                    "issue_key": "MTP-2",
                    "priority": "Medium",
                    "issue_type": "Bug",
                    "description": "test summary",
                    "component": "None",
                    "version": "None",
                    "bz_id": "964",
                    "comments": [],
                },
            },
            [
                {
                    "id": 2,
                    "bz_id": "None",
                    "product": "MTP",
                    "component": "None",
                    "summary": "test summary",
                    "version": "None",
                    "priority": "Medium",
                    "comments": [],
                    "bz_issue_type": "Bug",
                    "jira_counterpart": "MTP-1",
                },
                {
                    "id": 3,
                    "bz_id": "964",
                    "product": "MTP",
                    "component": "None",
                    "summary": "test summary",
                    "version": "None",
                    "priority": "Medium",
                    "comments": [],
                    "bz_issue_type": "Bug",
                    "jira_counterpart": "MTP-2",
                },
            ],
        )
    ],
)
def test_add_bugzilla_bug_ids_to_sync_log(payload, expected):
    """
    Test that the add_bugzilla_bug_ids_to_sync_log() function checks if it correctly adds new IDs to the new ID log and
    existing IDs to the existing ID log. Also checks if the payload is properly serialized
    :param payload: parsed JSON from Jira
    :param expected: serialized jira_json tickets to bz ticket
    """
    test_log = SyncLog.objects.create()
    result = add_bugzilla_bug_ids_to_sync_log(payload, test_log)
    new_jira_id_to_bz = ["MTP-1"]
    existing_jira_to_bz = ["MTP-2"]
    assert result == expected
    assert new_jira_id_to_bz == test_log.new_jira_to_bugzilla_ids
    assert existing_jira_to_bz == test_log.existing_jira_to_bugzilla_ids
