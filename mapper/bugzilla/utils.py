import requests
import os


TOKEN = os.environ.get("BUGZILLA_TOKEN")
URL = os.environ.get("BUGZILLA_URL")


def get_token():
    """
    Returns a token that the user then can set as a global environment variable for Bugzilla API calls
    """
    USERNAME = os.environ.get("BUGZILLA_URL")
    PASSWORD = os.environ.get("BUGZILLA_PASSWORD")
    URL = os.environ.get("BUGZILLA_URL")
    js = requests.get(URL + "/login?login=" + USERNAME + "&password=" + PASSWORD)
    token = js.json()["token"]
    return token
