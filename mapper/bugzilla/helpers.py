import concurrent.futures
import datetime

from mapper.bugzilla.models import BugzillaProduct, BugzillaTicket
from mapper.bugzilla.utils import *
from mapper.jira.serializers import ToBugzillaIssueSerializer
from mapper.jira.utils import update_jira_mirror_id


def get_latest_bugs(minutes_back=15):
    """
    Returns a dictionary of bugs from how many days back the user requests. Defaults to one day back.
    Args:
        days_back: Integer of how many days back to query
    """
    requested_time = datetime.datetime.now() - datetime.timedelta(minutes=minutes_back)
    default_product = str(BugzillaProduct.objects.get(name="Default").product_name)

    js = requests.get(
        URL + "/bug",
        params={
            "token": TOKEN,
            "creation_time": requested_time,
            "product": default_product,
        },
    )
    up = requests.get(
        URL + "/bug",
        params={
            "token": TOKEN,
            "last_change_time": requested_time,
            "product": default_product,
        },
    )
    bug_dict = js.json()
    bug_dict["bugs"] = up.json()["bugs"]

    return bug_dict


def parse_bugs(bugs, last_sync_time):
    """
    Returns a list of parsed Bugzilla bugs represented by dictionaries.
    Args:
        bugs: Bugzilla bugs.
        days_back: Positive integer of days back the user wants to query for Bugzilla updates.
    """

    date_format = "%Y-%m-%dT%H:%M:%fZ"

    bug_list = []
    if bugs:
        for bug in bugs["bugs"]:
            target_fields = {}
            target_fields.update({"id": bug["id"]})
            target_fields.update({"priority": bug["priority"]})
            target_fields.update({"product": bug["product"]})
            target_fields.update({"component": bug["component"]})
            target_fields.update({"summary": bug["summary"]})
            target_fields.update({"version": bug["version"]})
            target_fields.update({"cf_issue_type": bug["cf_issue_type"]})
            target_fields.update({"cf_jira_bug": bug["cf_jira_bug"]})
            target_fields.update({"last_change_time": bug["last_change_time"]})
            target_fields.update({"creation_time": bug["creation_time"]})

            updated_time = datetime.datetime.strptime(
                bug["last_change_time"], date_format
            )
            creation_time = datetime.datetime.strptime(
                bug["creation_time"], date_format
            )
            if updated_time > last_sync_time or creation_time > last_sync_time:
                bug_list.append(target_fields)

    return bug_list


def get_comments(bug, minutes_back=15):
    """
    Retrieves comment data for a bug in Bugzilla.
    Args:
        bug = Parsed Bugzilla bug.
        days_back: Number of days back to query on.
    """
    requested_time = datetime.datetime.now() - datetime.timedelta(
        minutes=int(minutes_back)
    )
    js = requests.get(
        URL + "/bug" + "/" + str(bug["id"]) + "/comment",
        params={"token": TOKEN, "new_since": requested_time},
    )

    bug["comments"] = [
        (comment["text"], comment["creator"])
        for comment in js.json()["bugs"][str(bug["id"])]["comments"]
        if comment["text"] != ""
    ]

    return bug


def get_comments_threading(parsed_issues, time_request=15):
    """
    Adds comments to Bugzilla JSON data.
    Args:
        parsed_issues: A list of all the parsed bugzilla issues.
        time_request: Number of days back queried on Bugzilla.
    """
    time = [time_request for _ in range(len(parsed_issues))]

    with concurrent.futures.ThreadPoolExecutor() as executor:
        bugs_with_comments = executor.map(get_comments, parsed_issues, time)
    return list(bugs_with_comments)


def create_bugs(bug):
    """
    Creates a new Bugzilla Bug via HTTP.
    Args:
        bug: JSON formatted Bugzilla bug data.
    """
    data = bug
    new_bug_id = requests.post(URL + "/bug" + "?token=" + TOKEN, data=data)

    return new_bug_id


def comment_json(comment):
    """
    Formats comment data from Jira.
    Args:
        comment: List of comments from Jira.
    """
    return {"token": TOKEN, "comment": comment, "is_markdown": False}


def add_comments(bug_id, comment_arr):
    """
    Posts comments to a Bugzilla bug.
    Args:
        bug_id: Id of the bug that is being updated.
        comment_arr: Comments being added to the bug.
    """
    comment_dic = {}
    for comment in comment_arr:
        try:
            data = comment_json(comment)
            test = requests.post(URL + "/bug/" + str(bug_id) + "/comment", data=data)
            comment_dic["succes_post"] = True
        except requests.exceptions.RequestException as e:
            comment_dic["succes_post"] = False
            comment_dic["error_notes"] = e

    return comment_dic


def convert_jira_issues(parsed_issues):
    """
    Takes parsed Jira data and creates Bugzilla objects to be serialized.

    Args:
        parsed_issues: Parsed issue data from Jira.
    """
    tickets = [
        BugzillaTicket.objects.create(
            product=issue["project_key"],
            component=issue["component"],
            summary=issue["description"],
            version=issue["version"],
            priority=issue["priority"],
            comments=issue["comments"],
            bz_issue_type=issue["issue_type"],
            bz_id=issue["bz_id"],
            jira_counterpart=issue["issue_key"],
        )
        for issue in parsed_issues.values()
    ]

    return [ToBugzillaIssueSerializer(ticket).data for ticket in tickets]


def reformat_data(bug):
    """
    Reformat data to be processable by Bugzilla.
    Args:
        bug:This is a bug from the jira array of jsons
    """

    if bug["priority"] == "Medium":
        bug["priority"] = "Normal"
    reformated = {
        "product": bug["product"],
        "component": "Test Component",
        "version": "unspecified",
        "summary": bug["summary"],
        "priority": bug["priority"],
        "cf_jira_bug": bug["jira_counterpart"],
        "cf_issue_type": bug["bz_issue_type"],
        "op_sys": "Linux",
        "platform": "PC",
    }
    return reformated


def get_buzilla_bug(bug_id):
    """
    Returns a dictionary of bugs from how many days back the user requests. Defaults to one day back.
    Args:
        days_back: Integer of how many days back to query
    """
    bugzilla_bug = requests.get(
        URL + "/bug", params={"token": TOKEN, "id": int(bug_id)}
    )

    return bugzilla_bug.json()


def comparison(bugzi_bug, jira_bug):
    """
    Comparison function for Bugzilla bug and Jira Issue. Returns a Bugzilla bug with updated Jira information.
    Args:
        bugzi_bug: Bugzilla bug
        jira_bug: Jira bug
    """
    bug = bugzi_bug[0]
    if bug["product"] != jira_bug["product"]:
        bug["product"] = jira_bug["product"]

    if bug["summary"] != jira_bug["summary"]:
        bug["summary"] = jira_bug["summary"]

    if bug["priority"] != jira_bug["priority"]:
        if bug["priority"] == "Medium":
            bug["priority"] = "Normal"
        else:
            bug["priority"] = jira_bug["priority"]

    if bug["cf_issue_type"] != jira_bug["bz_issue_type"]:
        bug["cf_issue_type"] = jira_bug["bz_issue_type"]

    return bug


def update(bug_id, bug_data):
    """
    Updates a Bugzilla bug.
    Args:
        bug_id: The bugzilla ID for the mapped Jira ID.
        bug_data: Data to be used to update.
    """
    bug_data["token"] = TOKEN
    update = requests.put(URL + "/bug/" + str(bug_id), data=bug_data)

    return update.json()


def post_all_bugs(jira_bug):
    """
    Posts bug data to Bugzilla.
    Args:
        jira_bug: This is a list of jira bug JSON.
    """
    bug_dic = {}
    if jira_bug != None:
        proper_data_fields = reformat_data(jira_bug)
        if jira_bug["bz_id"] == "None":
            try:
                created_id = create_bugs(proper_data_fields)
                id_dict = created_id.json()
                bz_id = (str)(id_dict["id"])
                update_jira_mirror_id(proper_data_fields["cf_jira_bug"], bz_id)
                bug_dic["success_sync"] = True
                sync_info = "created"
            except requests.exceptions.RequestException as e:
                bug_dic["success_sync"] = False
                bug_dic["error_notes"] = e

        else:
            try:
                bz_id = jira_bug["bz_id"]
                bz_bug = get_buzilla_bug(bz_id)
                parsed_bugs = parse_in_post(bz_bug)
                updated_bug = comparison(parsed_bugs, jira_bug)
                update(bz_id, updated_bug)
                bug_dic["success_sync"] = True
                sync_info = "updated"
            except requests.exceptions.RequestException as e:
                bug_dic["success_sync"] = False
                bug_dic["error_notes"] = e

        if jira_bug["comments"] != []:
            comment_dic = add_comments(bz_id, jira_bug["comments"])
            if comment_dic["succes_post"] == False:
                bug_dic["success_sync"] = False
                bug_dic["error_notes"] = comment_dic["error_notes"]

        bug_dic["id"] = bz_id
        bug_dic["sync_info"] = sync_info
    return bug_dic


def parse_in_post(bugs):
    """
        Returns a list of parsed Bugzilla bugs represented by dictionaries.
        Args:
            bugs: Bugzilla bugs.
            days_back: Positive integer of days back the user wants to query for Bugzilla updates.
        """

    date_format = "%Y-%m-%dT%H:%M:%fZ"

    bug_list = []
    if bugs:
        for bug in bugs["bugs"]:
            target_fields = {}
            target_fields.update({"id": bug["id"]})
            target_fields.update({"priority": bug["priority"]})
            target_fields.update({"product": bug["product"]})
            target_fields.update({"component": bug["component"]})
            target_fields.update({"summary": bug["summary"]})
            target_fields.update({"version": bug["version"]})
            target_fields.update({"cf_issue_type": bug["cf_issue_type"]})
            target_fields.update({"cf_jira_bug": bug["cf_jira_bug"]})
            target_fields.update({"last_change_time": bug["last_change_time"]})
            target_fields.update({"creation_time": bug["creation_time"]})

            bug_list.append(target_fields)
        return bug_list


def post_all_bugs_threading(jira_bug_list):
    """
    Returns log of created and updated bugs from Bugzilla.
    Args:
        jira_bug_list:  a list of parsed jira bugs
    """

    with concurrent.futures.ThreadPoolExecutor() as executor:
        sync_log = executor.map(post_all_bugs, jira_bug_list)
    return list(sync_log)


def bz_update_jira(bz_id, jira_id):
    """
    Updates a Bugzilla bug with a mirror id from Jira.
    Args:
        bz_id:  Bugzilla bug's id
        jira_id: Jira issue's id
    """
    bug_data = {
        "token": TOKEN,
        "cf_jira_bug": jira_id,
    }
    updated = requests.put(URL + "/bug/" + str(bz_id), data=bug_data)

    return updated.json()


def add_bugzilla_bug_ids_to_sync_log(jira_json, synclog):
    """
    will take the jira issues seralize them and then add to the logs
    :param jira_json: parsed jira list
    :param synclog: synclog object
    :return: seralized jira bugs
    """

    seralized_jira_bugs_to_bz = convert_jira_issues(jira_json)
    new_tickets = [
        ticket for ticket in seralized_jira_bugs_to_bz if ticket["bz_id"] == "None"
    ]
    update_tickets = [
        ticket for ticket in seralized_jira_bugs_to_bz if ticket not in new_tickets
    ]
    synclog.new_jira_to_bugzilla_ids = [
        ticket["jira_counterpart"] for ticket in new_tickets
    ]
    synclog.existing_jira_to_bugzilla_ids = [
        ticket["jira_counterpart"] for ticket in update_tickets
    ]
    return seralized_jira_bugs_to_bz
