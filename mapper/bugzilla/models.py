from django.contrib.postgres.fields import ArrayField
from django.db import models


class BugzillaTicket(models.Model):
    # bugzilla Id
    bz_id = models.CharField(max_length=64)
    # the project the bug was found in
    product = models.CharField(max_length=64)
    # name of the component in the product above
    component = models.CharField(max_length=64)
    # brief description of bug
    summary = models.CharField(max_length=30000)
    # the version of product the bug was found in
    version = models.CharField(max_length=64)
    priority = models.CharField(max_length=64)
    # a bug can have no more than 100 comments per created or updated bug
    comments = ArrayField(models.CharField(max_length=2000, blank=True), size=100)
    # this is a unique field that would add the type to the issue (task,bug, epic)
    bz_issue_type = models.CharField(max_length=64)
    # this is the sixth field that is the jira id
    jira_counterpart = models.CharField(max_length=64)


class BugzillaProduct(models.Model):
    product_name = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
