from django.urls import path
from .views import GetLatestBugs, SyncBugs

app_name = "bugzilla"

urlpatterns = [
    path("/changes", GetLatestBugs.as_view()),
    path("/changes/<str:mins_back>", GetLatestBugs.as_view()),
    path("/sync/<str:mins_back>", SyncBugs.as_view()),
    path("/sync", SyncBugs.as_view()),
]
