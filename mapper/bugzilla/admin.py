from django.contrib import admin

from mapper.bugzilla.models import BugzillaProduct


class BugzillaProductAdmin(admin.ModelAdmin):
    model = BugzillaProduct
    list_display = ("name", "product_name")


admin.site.register(BugzillaProduct, BugzillaProductAdmin)
