from rest_framework import serializers

from mapper.jira.models import JiraTicket


class ToJiraIssueSerializer(serializers.ModelSerializer):
    class Meta:
        model = JiraTicket
        fields = "__all__"
