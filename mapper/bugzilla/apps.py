from django.apps import AppConfig


class BugzillaConfig(AppConfig):
    name = "mapper.bugzilla"
