from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from mapper.bugzilla.helpers import *
from mapper.jira.utils import *


class GetLatestBugs(APIView):
    def get(self, request, *args, **kwargs):
        time_request = int(kwargs.get("mins_back", 15))

        if time_request < 0:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        default_log_filter = datetime.datetime.now() - datetime.timedelta(
            days=int(time_request)
        )
        latest_issues = get_latest_bugs(minutes_back=time_request)
        parsed_issues = parse_bugs(latest_issues, default_log_filter)

        bug_with_new_comments = get_comments_threading(parsed_issues, time_request)

        return Response(bug_with_new_comments)


class SyncBugs(APIView):
    def get(self, request, *args, **kwargs):
        jira_time_request = int(kwargs.get("mins_back", 1))
        if jira_time_request < 0:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        jira_time_request = 0 - jira_time_request
        jira_latest_bugs = get_latest_issues(jira_project=monarch_test_jira)
        parsed_jira_bugs = parse_issues(
            jira_project=monarch_test_jira, issues_in_project=jira_latest_bugs
        )

        jira_bugs_in_bugzilla_tickets_form = convert_jira_issues(parsed_jira_bugs)
        log = post_all_bugs_threading(jira_bugs_in_bugzilla_tickets_form)

        return Response(log)
