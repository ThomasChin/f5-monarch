import pytest

from mapper.logs.models import SyncLog


@pytest.fixture()
def admin_user_data():
    return {
        "email": "charlie.b@f5.com",
        "name": "Charlie Brown",
        "nickname": "Chuck",
        "password": "littleredhairedgirl",
        "is_staff": True,
    }


@pytest.fixture()
def admin_user(django_user_model, admin_user_data):
    return django_user_model.objects.create_user(**admin_user_data)


@pytest.fixture()
def admin_client(client, admin_user_data, admin_user):
    client.login(email=admin_user_data["email"], password=admin_user_data["password"])
    return client


@pytest.fixture()
def testing_synclog():
    return SyncLog.objects.create()
