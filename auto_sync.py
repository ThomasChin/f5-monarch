import django
import requests
import schedule
import time
import os

# Need to setup Django before you can import models. Apps must be loaded first.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mapper.settings")
django.setup()

from mapper.logs.models import SyncFrequency


def mapper_sync():
    """
	Makes a request to the Mapper Sync Endpoint and begins a new sync.
	"""
    print("running!")
    r = requests.get("http://localhost:8000/api/v1/sync")
    print(r.status_code)


sync_frequency = SyncFrequency.objects.get(
    name="Default"
).frequency_by_minutes  # Default is 15 minutes.
print("Sync will run every: " + str(sync_frequency) + " minutes.")
schedule.every(sync_frequency).minutes.do(mapper_sync)

while True:
    schedule.run_pending()
    print("Next Sync at: ")
    print(schedule.next_run())
    time.sleep(1)
